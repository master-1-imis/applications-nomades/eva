package com.example.eva.service.api.google.calendar;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.work.BackoffPolicy;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.ForegroundInfo;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.eva.Environment;
import com.example.eva.R;
import com.example.eva.database.association.Association;
import com.example.eva.database.association.AssociationViewModel;
import com.example.eva.database.event.Event;
import com.example.eva.database.event.EventViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class DataSynchronisationWorker extends Worker {

	// -------------------- ATTRIBUTE
	private final static String NOTIFICATION_CHANNEL_ID = "1";
	private final static String NOTIFICATION_ID = "1";
	private final Context context;
	private final AssociationViewModel associationViewModel;
	private final EventViewModel eventViewModel;
	private final SimpleDateFormat dateFormatRequest;
	private final SimpleDateFormat dateFormatResponse1;
	private final SimpleDateFormat dateFormatResponse2;
	private final SimpleDateFormat dateFormatResponse3;
	private final NotificationManager notificationManager;
	private List<Event> events;
	private boolean isConnected;


	// -------------------- CONSTRUCTOR
	@SuppressLint("SimpleDateFormat")
	public DataSynchronisationWorker(@NonNull Context context, @NonNull WorkerParameters params) {
		super(context, params);
		this.context = context;
		this.associationViewModel = new AssociationViewModel((Application) context);
		this.eventViewModel = new EventViewModel((Application) context);
		this.dateFormatRequest = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		this.dateFormatResponse1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		this.dateFormatResponse2 = new SimpleDateFormat("yyyy-MM-dd");
		this.dateFormatResponse3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'");
		this.notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		this.events = new ArrayList<>();
		this.isConnected = checkConnection(context);
	}


	// -------------------- OVERRIDE METHOD
	@NonNull
	@Override
	public Result doWork() {
		if (this.isConnected) {
			// début d'affichage de la notification
			setForegroundAsync(createForegroundInfo(true, R.string.notification_sync_progress_description));

			// téléchargement pour une association
			if (getInputData().getBoolean("onlyOne", false)) {
				JSONArray jsonItems = this.download(getInputData().getString("idGoogleCalendar"));
				this.transformRessources(getInputData().getInt("id", 0), jsonItems);
			}

			// téléchargement pour plusieurs associations
			else {
				List<Association> associations = this.associationViewModel.getSubscriberList();
				int iterator = 0;
				while (this.isConnected && iterator<associations.size()) {
					Association association = associations.get(iterator);
					JSONArray jsonItems = this.download(association.getIdGoogleCalendar());
					this.transformRessources(association.getId(), jsonItems);
					iterator++;
				}
			}

			// fin d'afficahge de la notification
			setForegroundAsync(createForegroundInfo(false, R.string.notification_sync_complete_description));

			// enregistrement des événements en base de données
			if (this.events.size() > 0) { this.eventViewModel.insertUpdate(this.events); }
		}
		if (!this.isConnected) { return Result.failure(); }
		if (getInputData().getBoolean("periodic", false)) { return Result.retry(); }
		return Result.success();
	}


	// -------------------- CLASS METHOD
	/**
	 * Télécharge depuis internet via l'API Google Agenda les ressources de type événement d'une association.
	 * @param calendarId L'identifiant d'un agenda Google public.
	 * @return La liste des prochains événements à partir de la date actuelle à minuit.
	 */
	private JSONArray download(String calendarId) {
		JSONArray jsonItems = new JSONArray();
		Calendar today = new GregorianCalendar();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		try {
			URL url = new URL("https://www.googleapis.com/calendar/v3/calendars/" + calendarId + "/events?key=AIzaSyDerkHY8MaM_fPq51TidS2ZE3pnGoX2fb0&timeMin=" + this.dateFormatRequest.format(today.getTime()) + "&showDeleted=true");
			URLConnection connection = url.openConnection();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder responseBuilder = new StringBuilder();
			String line = bufferedReader.readLine();
			responseBuilder.append(line);

			while (line != null) {
				line = bufferedReader.readLine();
				responseBuilder.append(line);
			}

			jsonItems = new JSONObject(responseBuilder.toString()).getJSONArray("items");
		}
		catch (IOException | JSONException e) {

			// si une erreur de résolution, alos réévaluation de l'état de connexion à internet.
			if (e.getClass().getName().equals(UnknownHostException.class.getName())) {
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) { this.isConnected = checkConnection(this.context); }
				else{ this.isConnected = false; }
			}
			else {
				e.printStackTrace();
			}
		}
		return jsonItems;
	}


	/**
	 * Transforme les ressources JSON en Event.
	 * @param idAssociation L'identifiant de l'agenda.
	 * @param jsonItems La liste des ressources JSON d'un agenda.
	 */
	private void transformRessources(int idAssociation, JSONArray jsonItems) {
		for (int i=0; i<jsonItems.length(); i++) {
			try {
				JSONObject jsonEvent = jsonItems.getJSONObject(i);
				this.events.add(
						new Event(
								idAssociation,
								jsonEvent.getString("id"),
								jsonEvent.getString("summary"),
								jsonEvent.getString("status").compareTo("cancelled") == 0,
								this.parseDate(jsonEvent, "start"),
								this.parseDate(jsonEvent, "end"),
								this.parseDate(jsonEvent, "updated"),
								jsonEvent.has("location") ? jsonEvent.getString("location") : null,
								jsonEvent.has("description") ? jsonEvent.getString("description") : null
						)
				);
			}
			catch (JSONException e) { e.printStackTrace(); }
		}
	}


	/**
	 * Récupère une date à partir de trois formats de date différents.
	 * @param jsonEvent La ressource à parser.
	 * @param dateSection la clé d'un élément de la ressource.
	 * @return La date de la section souhaité.
	 */
	private Date parseDate(JSONObject jsonEvent, String dateSection) {
		Date date = new Date();
		try {
			// si c'est la datetime de mise à jour
			if (dateSection.equals("updated")) {
				date = this.dateFormatResponse3.parse(jsonEvent.getString(dateSection));
			}
			// si c'est la datetime de début ou de fin
			else if (jsonEvent.getJSONObject(dateSection).has("dateTime")) {
				date = this.dateFormatResponse1.parse(jsonEvent.getJSONObject(dateSection).getString("dateTime"));
			}
			// si c'est juste la date de début ou de fin
			else if (jsonEvent.getJSONObject(dateSection).has("date")) {
				date = this.dateFormatResponse2.parse(jsonEvent.getJSONObject(dateSection).getString("date"));
			}
		}
		catch (JSONException | ParseException e) { e.printStackTrace(); }
		return date;
	}


	/**
	 * Crée une notification de progression du téléchargement des ressources.
	 * @param inprogress La valeur logique définit si le téléchargement est en cours de progression ou si elle est terminé.
	 * @param descriptionRessourceId L'identifiant de la ressource dans `string.xml` qui d'écrit l'état actuelle de la tâche.
	 * @return Un ensemble d'information utilisé.
	 */
	@NonNull
	private ForegroundInfo createForegroundInfo(boolean inprogress, int descriptionRessourceId) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			createNotificationChannel();
		}

		Notification notification = new NotificationCompat.Builder(this.context, NOTIFICATION_CHANNEL_ID)
				.setSmallIcon(R.drawable.ic_sync)
				.setContentTitle(this.context.getString(R.string.notification_channel_name))
				.setContentText(this.context.getString(descriptionRessourceId))
				.setPriority(NotificationCompat.PRIORITY_LOW)
				.setProgress(0, 0, inprogress)
				.setCategory(NotificationCompat.CATEGORY_PROGRESS)
				.setOnlyAlertOnce(true)
				.build();

		return new ForegroundInfo(Integer.parseInt(NOTIFICATION_ID), notification);
	}


	/**
	 * Cré un salon de notification pour l'application.
	 */
	@RequiresApi(api = Build.VERSION_CODES.O)
	private void createNotificationChannel() {
		NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, this.context.getString(R.string.notification_channel_name), NotificationManager.IMPORTANCE_DEFAULT);
		channel.enableVibration(false);
		this.notificationManager.createNotificationChannel(channel);
	}


	// -------------------- STATIC METHOD
	/**
	 * Lance la synchronisation des données pour toutes les associations suivis ou pour une seule association et le paramètre est non `null`.
	 * @param context Le contexte d'exécution.
	 * @param association Une association.
	 */
	public static void sheduler(@NonNull Context context, @Nullable Association association) {
		if (checkConnection(context)) {
			Data.Builder builder = new Data.Builder();
			builder.putBoolean("onlyOne", association != null);
			if (association != null) { builder.putInt("id", association.getId()).putString("idGoogleCalendar", association.getIdGoogleCalendar()); }

			OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(DataSynchronisationWorker.class)
					.setConstraints(new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build())
					.addTag("data_sync")
					.setInputData(builder.build())
					.build();

			WorkManager.getInstance(context).enqueueUniqueWork("sync", ExistingWorkPolicy.APPEND_OR_REPLACE, workRequest);
		}
	}


	/**
	 * Lance une synchronisation des données pour toutes les associations suivis toutes les 5h pour mettre à jour l'application en arrière-plans.
	 * @param context Le contexte d'exécution.
	 */
	public static void shedulerPeriodic(@NonNull Context context) {
		WorkManager.getInstance(context).cancelAllWorkByTag("periodic_data_sync");

		OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(DataSynchronisationWorker.class)
				.setConstraints(new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build())
				.addTag("periodic_data_sync")
				.setInputData(new Data.Builder().putBoolean("onlyOne", false).putBoolean("periodic", true).build())
				.setBackoffCriteria(BackoffPolicy.LINEAR, WorkRequest.MAX_BACKOFF_MILLIS, TimeUnit.MILLISECONDS)
				.build();

		WorkManager.getInstance(context).enqueueUniqueWork("periodic_sync", ExistingWorkPolicy.KEEP, workRequest);
	}


	/**
	 * Vérifie si une connexion internet est établie.
	 * @param context Le contexte d'éxécution.
	 * @return `true` si le Wifi ou le GSM permet de sortir sur internet et `false` dans le cas contraire.
	 */
	private static boolean checkConnection(Context context) {
		boolean connected = true;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			ConnectivityManager connectivityManager = context.getSystemService(ConnectivityManager.class);
			Network network = connectivityManager.getActiveNetwork();

			if (network != null) { // test si le GSM ou le wifi ou [...] est activé.
				NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
				connected = Objects.requireNonNull(networkCapabilities).hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET); // test si il est possible d'aller sur internet.
			}
			else { connected = false; }
		}
		if (!connected) { ((Environment)context).showToast(R.string.no_connection); }
		return connected;
	}
}
