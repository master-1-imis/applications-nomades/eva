package com.example.eva;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.preference.PreferenceManager;

import com.example.eva.database.association.Association;
import com.example.eva.service.api.google.calendar.DataSynchronisationWorker;
import com.google.android.material.navigation.NavigationView;

import java.util.Objects;

public class MainActivity extends Environment {

	// -------------------- ATTRIBUTE
	private AppBarConfiguration mAppBarConfiguration;
	private static final String PREFERENCE_POST_INSTALLATION = "FIRST_TIME_AFTER_INSTALLATION";


	// -------------------- OVERRIDE METHOD
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// mise en place de la barre d'action
		setContentView(R.layout.activity_main);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		// initialisation du menu de navigation
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		NavigationView navigationView = findViewById(R.id.nav_view);
		mAppBarConfiguration = new AppBarConfiguration.Builder(
				R.id.nav_filter_week,
				R.id.nav_filter_month,
				R.id.nav_short_association,
				R.id.nav_subscriber_association
		).setOpenableLayout(drawer).build();

		NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
		NavController navController = Objects.requireNonNull(navHostFragment).getNavController();
		NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
		NavigationUI.setupWithNavController(navigationView, navController);

		// Préférences de démarrage
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		if (preferences.getBoolean(PREFERENCE_POST_INSTALLATION, true)) {
			this.loadData(); // chargement du jeux de test
			SharedPreferences.Editor editor = preferences.edit();
			editor.putBoolean(PREFERENCE_POST_INSTALLATION, false);
			editor.apply();
		}

		// synchronisation des données en tâche de fond depuis l'API Google Agenda
		DataSynchronisationWorker.shedulerPeriodic(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		if(item.getItemId()== R.id.action_bar_sync) {
			DataSynchronisationWorker.sheduler(this, null);
		}
		return false;
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.action_bar, menu);
		return true;
	}


	@Override
	public boolean onSupportNavigateUp() {
		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
		return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
	}


	// -------------------- CLASS METHOD
	/**
	 * Charge en base de données un jeux de teste.
	 */
	private void loadData() {
		// création des associations.
		this.associationViewModel.insert(new Association("FIFO", "rss10msnvjieg58tcna32dki4c@group.calendar.google.com", R.mipmap.ic_logo_fifo,true));
		this.associationViewModel.insert(new Association("INFASSO", "edjm86nf7acjfq3gbvrmhur6hg@group.calendar.google.com", R.mipmap.ic_logo_infasso,false));
		this.associationViewModel.insert(new Association("Tribu Terre","gu6jlhjui9s34ede58s3kub6ec@group.calendar.google.com", R.mipmap.ic_logo_tribu_terre,false));
		this.associationViewModel.insert(new Association("AMIGO","au47go9j2h1ugt7rl98j9qdh7s@group.calendar.google.com", R.mipmap.ic_logo_amigo,false));
	}

}