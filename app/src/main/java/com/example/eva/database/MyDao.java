package com.example.eva.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.eva.database.association.Association;
import com.example.eva.database.event.Event;

import java.util.List;


@Dao
public interface MyDao {

	//----------------------------------------//
	//  EVENEMENT                             //
	//----------------------------------------//
	// -------------------- Insert
	@Insert
	void insertEvent(Event event);


	// -------------------- Update
	@Update
	void updateEvent(Event event);


	// -------------------- Delete
	@Delete
	void deleteEvent(Event event);

	@Query("DELETE FROM event_table WHERE dateEnd < :now AND dateUpdated < :now")
	void deleteOldEvents(long now);


	// -------------------- Get
	@Query("SELECT * FROM event_table WHERE id == :id ")
	Event getEvent(int id);

	@Query("SELECT * FROM event_table WHERE idGoogleEvent == :idGoogleEvent")
	Event getEvent(String idGoogleEvent);


	// -------------------- Get Live Data
	@Query("SELECT * FROM event_table ORDER BY dateStart ASC")
	LiveData<List<Event>> getEventData();


	// -------------------- Get List
	@Query("SELECT * FROM event_table ORDER BY dateStart ASC")
	List<Event> getAllEventList();

	@Query("SELECT * FROM event_table WHERE dateEnd >= :now AND dateStart < :limit AND dateStart <= dateEnd AND idAssociation IN (SELECT id FROM association_table WHERE subscribe=1) ORDER BY dateStart ASC")
	List<Event> getAllEventBeforeLimit(long now, long limit);

	@Query("SELECT * FROM event_table WHERE dateEnd >= :now AND idAssociation == :id ORDER BY dateStart ASC")
	List<Event> getAllEventByAssociation(long now, int id);


	// -------------------- Get Other
	@Query("SELECT count(*) FROM event_table WHERE dateEnd >= :now AND idAssociation == :id AND canceled = 0 ORDER BY dateStart ASC")
	int numberEventConfirmedByAssociation(long now, int id);



	//----------------------------------------//
	//  ASSOCIATION                           //
	//----------------------------------------//
	// -------------------- Insert
	@Insert
	void insertAssociation(Association association);


	// -------------------- Update
	@Update
	void updateAssociation(Association association);


	// -------------------- Delete
	@Delete
	void deleteAssociation(Association association);


	// -------------------- Get
	@Query("SELECT * FROM association_table WHERE id == :id")
	Association getAssociation(int id);

	@Query("SELECT * FROM association_table WHERE idGoogleCalendar == :idGoogleCalendar")
	Association getAssociation(String idGoogleCalendar);


	// -------------------- Get List
	@Query("SELECT * FROM association_table  ORDER BY name ASC")
	List<Association> getAllAssociationList();

	@Query("SELECT * FROM association_table where subscribe = 1  ORDER BY name ASC")
	List<Association> getAllAssociationSubscribe();
}
