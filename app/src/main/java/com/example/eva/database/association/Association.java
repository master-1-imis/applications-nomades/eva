package com.example.eva.database.association;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "association_table")
public class Association {

	// -------------------- ATTRIBUTE
	/**
	 * Identifiant de l'association.
	 */
	@PrimaryKey(autoGenerate = true)
	private int id;

	/**
	 * Identifiant du Google agenda public.
	 */
	@NonNull
	@ColumnInfo(name = "idGoogleCalendar")
	private String idGoogleCalendar;

	/**
	 * Nom de l'association.
	 */
	@NonNull
	@ColumnInfo(name = "name")
	private String name;

	/**
	 * Identifiant de ressource correspondant au logo de l'association.
	 */
	@Nullable
	@ColumnInfo(name = "logo")
	private Integer logo;

	/**
	 * État de suivis de l'association.
	 */
	@NonNull
	@ColumnInfo(name = "subscribe")
	private Boolean subscribe;


	// -------------------- CONSTRUCTOR
	/**
	 * Constructeur de l'association
	 * @param name Le nom de l'association.
	 * @param idGoogleCalendar L'identifiant du Google agenda public.
	 * @param logo L'identifiant de la ressource image du logo.
	 * @param subscribe L'état de suivi de l'association.
	 */
	public Association(@NonNull String name, @NonNull String idGoogleCalendar, @Nullable Integer logo, @NonNull Boolean subscribe) {
		this.name = name;
		this.idGoogleCalendar = idGoogleCalendar;
		this.logo = logo;
		this.subscribe = subscribe;
	}


	// -------------------- GET
	public int getId() {
		return this.id;
	}

	@NonNull
	public String getIdGoogleCalendar() {
		return this.idGoogleCalendar;
	}

	@NonNull
	public String getName() {
		return this.name;
	}

	@Nullable
	public Integer getLogo() {
		return this.logo;
	}

	@NonNull
	public Boolean getSubscribe() {
		return this.subscribe;
	}


	// -------------------- SET
	public void setId(int id) {
		this.id = id;
	}

	public Association setIdGoogleCalendar(@NonNull String idGoogleCalendar) {
		this.idGoogleCalendar = idGoogleCalendar;
		return this;
	}

	public Association setName(@NonNull String name) {
		this.name = name;
		return this;
	}

	public Association setLogo(@Nullable Integer logo) {
		this.logo = logo;
		return this;
	}

	public Association setSubscribe(@NonNull Boolean subscribe) {
		this.subscribe = subscribe;
		return this;
	}


	// -------------------- OVERRIDE
	@NonNull
	@Override
	public String toString() {
		return "Association {" + " id=" + this.id + ", name='" + this.name + "'" + ", idGoogleCalendar=" + this.idGoogleCalendar + ", logo='" + logo + "'" + ", subscribe=" + subscribe + '}';
	}

}

