package com.example.eva.database.event;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.eva.database.association.Association;

import java.util.List;



public class EventViewModel extends AndroidViewModel {

	// -------------------- ATTRIBUTE
	private final EventRepository repository;
	private final LiveData<List<Event>> data;

	/**
	 * CONSTRUCTEUR
	 * @param application l'application
	 */
	public EventViewModel(Application application) {
		super(application);
		this.repository = new EventRepository(application);
		this.data = this.repository.getData();
	}


	/**
	 * L'ajout et la mise à jour d'une liste d'évènement
	 * @param events la liste d'évènement
	 */
	public void insertUpdate(List<Event> events) {
		this.repository.insertUpdate(events);
	}


	/**
	 * Supprimer un évènement
	 * @param event un évènement
	 */
	public void remove(Event event) {
		this.repository.delete(event);
	}

	/**
	 * Supprimer tous les évènements passés
	 */
	public void deleteOldEvents() {
		this.repository.deleteOldEvents();
	}


	/**
	 * Donner un évènement en fonction de son identifiant
	 * @param id l'identifiant de l'évènement
	 * @return un évènement
	 */
	public Event get(int id) {
		return this.repository.getEvent(id);
	}

	/**
	 * Donner un évènement en fonction de son identifiant de google aganda
	 * @param idGoogleEvent l'identifiant de l'évènement dans google aganda
	 * @return un évènement
	 */
	public Event get(String idGoogleEvent) {
		return this.repository.getEvent(idGoogleEvent);
	}

	/**
	 * Donner l'observateur sur la liste des événements
	 * @return l'observateur sur la liste des événements
	 */
	public LiveData<List<Event>> getData() {
		return this.data;
	}


	/**
	 * La liste de tous les évènements
	 * @return la liste de tous les évènements
	 */
	public List<Event> getList() {
		return this.repository.getAllEventList();
	}

	/**
	 * La liste de tous les évènements entre la date d'aujourd'hui et la date dans un certain nombre de jours
	 * @param daysNumber le nombre de jours
	 * @return le liste des évènements
	 */
	public List<Event> getListBeforeLimit(int daysNumber) {
		return this.repository.getAllEventBeforeLimit(daysNumber);
	}

	/**
	 * la liste des évènements d'une association
	 * @param association association
	 * @return la liste des évènements
	 */
	public List<Event> getListByAssociation(Association association) {
		return this.repository.getAllEventListByAssociation(association);
	}


	/**
	 * Le nombre d'évènement confirmé pour une association
	 * @param association une association
	 * @return Le nombre d'évènement confirmé pour une association
	 */
	public int numberConfirmed(Association association) {
		return this.repository.numberEventConfirmedByAssociation(association);
	}
}


