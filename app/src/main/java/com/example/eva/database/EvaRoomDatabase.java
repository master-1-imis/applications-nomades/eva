package com.example.eva.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.example.eva.database.association.Association;
import com.example.eva.database.event.Event;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Creation de 2 tables dans la BD : Association et Event
 */
@Database(entities = {Association.class, Event.class}, version = 3, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class EvaRoomDatabase extends RoomDatabase {
    public abstract MyDao myDao();

    // Creation d'une instance de la base de données
    private static EvaRoomDatabase INSTANCE;
    public static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(4);

    /**
     * Recuperation de la BD
     * @param context le contexte
     * @return la BD
     */
    public static EvaRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (EvaRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), EvaRoomDatabase.class, "eva_database").fallbackToDestructiveMigration().build();
                }
            }
        }
        return INSTANCE;
    }
}
