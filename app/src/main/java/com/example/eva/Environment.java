package com.example.eva;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.eva.database.association.AssociationViewModel;
import com.example.eva.database.event.EventViewModel;

public class Environment extends AppCompatActivity {

	// -------------------- ATTRIBUTES
	protected EventViewModel eventViewModel;
	protected AssociationViewModel associationViewModel;
	private static volatile Environment instance;


	// -------------------- OVERRIDE METHOD
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.eventViewModel = new ViewModelProvider(this).get(EventViewModel.class);
		this.associationViewModel = new ViewModelProvider(this).get(AssociationViewModel.class);
	}

	// -------------------- STATIC METHOD
	/**
	 * Récupère une instance de l'activity Environnement.
	 * @return Un environnement
	 */
	public static Environment getInstance() {
		if (instance == null) {
			synchronized (Environment.class) {
				if (instance == null) {
					instance = new Environment();
				}
			}
		}
		return instance;
	}


	// -------------------- CLASS METHOD
	/**
	 * Donne accès au modèle de gestion des événements.
	 * @return Retourne un objet non null du modèle de gestion des événements.
	 */
	@NonNull
	public EventViewModel getEventViewModel() {
		if (this.eventViewModel == null) { this.eventViewModel = new ViewModelProvider(this).get(EventViewModel.class); }
		return this.eventViewModel;
	}


	/**
	 * Donne accès au modèle de gestion des associations.
	 * @return Retourne un objet non null du modèle de gestion des associations.
	 */
	@NonNull
	public AssociationViewModel getAssociationViewModel() {
		if (this.associationViewModel == null) { this.associationViewModel = new ViewModelProvider(this).get(AssociationViewModel.class); }
		return this.associationViewModel;
	}


	/**
	 * Affiche un Toast à l'écran
	 * @param ressourceId l'identifiant de la ressource textuelle à afficher.
	 */
	public void showToast(final int ressourceId) {
		final Environment activity = this;
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(activity, ressourceId, Toast.LENGTH_LONG).show();
			}
		});
	}
}
