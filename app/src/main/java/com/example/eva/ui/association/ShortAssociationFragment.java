package com.example.eva.ui.association;

import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva.Environment;
import com.example.eva.R;
import com.example.eva.database.association.Association;

import java.util.List;
import java.util.Objects;

public class ShortAssociationFragment extends Fragment {

	// -------------------- ATTRIBUTE
	private int columnCount;
	private Environment activity;


	// -------------------- CONSTRUCTOR
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ShortAssociationFragment() {}


	// -------------------- OVERRIDE METHOD
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Environment activity = (Environment) this.requireParentFragment().getActivity();
		this.columnCount = 2;
		this.activity = activity;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View layout = inflater.inflate(R.layout.fragment_short_association_list, container, false);
		final RecyclerView recyclerView = layout.findViewById(R.id.list);
		final ConstraintLayout information_no_subscribe = layout.findViewById(R.id.information_no_subsciption);

		// action sur le bouton pour aller sur le fragment d'abonnement
		information_no_subscribe.findViewById(R.id.information_no_subsciption_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NavHostFragment navHostFragment = (NavHostFragment) activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
				Objects.requireNonNull(navHostFragment).getNavController().navigate(R.id.nav_subscriber_association);
			}
		});

		// adapter le nombre de colonne en fonction de la taille d'écran
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
			int width = this.activity.getWindowManager().getCurrentWindowMetrics().getBounds().width();
			this.columnCount = (int) Math.ceil((double) width / 360);
		}
		else {
			Point point = new Point();
			this.activity.getWindowManager().getDefaultDisplay().getSize(point);
			this.columnCount = (int) Math.ceil((double) point.x / 360);
		}

		List<Association> associations = this.activity.getAssociationViewModel().getSubscriberList();

		// si aucune association n'est suivie
		if (associations.size() == 0) {
			information_no_subscribe.setVisibility(View.VISIBLE);
			recyclerView.setVisibility(View.INVISIBLE);
		}

		// sinon
		else {
			information_no_subscribe.setVisibility(View.INVISIBLE);
			recyclerView.setVisibility(View.VISIBLE);
			recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), this.columnCount));
			recyclerView.setAdapter(new ShortAssociationRecyclerViewAdapter(associations, this.activity));
		}
		return layout;
	}
}
