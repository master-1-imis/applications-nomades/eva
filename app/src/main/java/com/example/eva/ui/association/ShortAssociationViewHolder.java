package com.example.eva.ui.association;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva.Environment;
import com.example.eva.EventsAssociationActivity;
import com.example.eva.R;
import com.example.eva.database.association.Association;

public class ShortAssociationViewHolder extends RecyclerView.ViewHolder {

	public final View view;
	public final TextView nameView;
	public final ImageView logoView;
	public final TextView counterView;
	public final ImageView couterBackgroundView;
	public Association association;


	public ShortAssociationViewHolder(@NonNull View itemView, final Environment activity) {
		super(itemView);
		this.view = itemView;
		this.nameView = itemView.findViewById(R.id.association_name);
		this.logoView = itemView.findViewById(R.id.association_icon);
		this.counterView = itemView.findViewById(R.id.association_event_counter);
		this.couterBackgroundView = itemView.findViewById(R.id.association_event_background);

		this.view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(activity, EventsAssociationActivity.class);
				intent.putExtra(EventsAssociationActivity.keyIdAssociation,association.getId());
				activity.startActivityForResult(intent, 0);
                activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
			}
		});
	}


	@NonNull
	@Override
	public String toString() {
		return super.toString() + " AssociationViewHolder{ nameView=" + this.nameView + "}";
	}
}
