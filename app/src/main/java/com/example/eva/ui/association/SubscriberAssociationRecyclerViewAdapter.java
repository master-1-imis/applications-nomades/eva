package com.example.eva.ui.association;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva.Environment;
import com.example.eva.R;
import com.example.eva.database.association.Association;

import java.util.List;

public class SubscriberAssociationRecyclerViewAdapter extends RecyclerView.Adapter<SubscriberAssociationViewHolder> {

    private final Environment activity;
    private final List<Association> associations;


    public SubscriberAssociationRecyclerViewAdapter(Environment activity) {
        this.activity = activity;
        this.associations = activity.getAssociationViewModel().getList();
    }


    @NonNull
    @Override
    public SubscriberAssociationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_subscriber_association, parent, false);
        return new SubscriberAssociationViewHolder(view, this.activity);
    }


    @Override
    public void onBindViewHolder(final SubscriberAssociationViewHolder holder, int position) {
        Association association = this.associations.get(position);
        holder.association = association;
        holder.subscribeView.setText(association.getName());
        holder.subscribeView.setChecked(association.getSubscribe());
        if (association.getLogo() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.logoView.setForeground(null);
            }
            holder.logoView.setImageResource(association.getLogo());
        }
    }


    @Override
    public int getItemCount() {
        return this.associations.size();
    }
}
