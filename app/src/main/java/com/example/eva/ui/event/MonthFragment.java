package com.example.eva.ui.event;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva.Environment;
import com.example.eva.R;
import com.example.eva.database.event.Event;
import com.example.eva.database.event.EventViewModel;

import java.util.List;
import java.util.Objects;


public class MonthFragment extends Fragment {

	// -------------------- ATTRIBUT
	public static final int PERIOD = 30; // 30 prochain jours
	public EventViewModel modelEvent;
	private Environment activity;


	// -------------------- CONSTRUCTOR
	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public MonthFragment() {}


	// -------------------- OVERRIDE METHOD
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Environment activity = (Environment) this.requireParentFragment().getActivity();
		this.activity = activity;
		this.modelEvent = Objects.requireNonNull(activity).getEventViewModel();
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View layout = inflater.inflate(R.layout.fragment_event_month_list, container, false);
		final RecyclerView recyclerView = layout.findViewById(R.id.list);
		final ConstraintLayout information_no_subscribe = layout.findViewById(R.id.information_no_subsciption);
		final ConstraintLayout information_no_event = layout.findViewById(R.id.information_no_event);

		// ajout d'un observateur pour mettre à jour l'interface sans changer de fragment.
		this.modelEvent.getData().observe(this.activity, new Observer<List<Event>>() {
			@Override
			public void onChanged(List<Event> items) {
				List<Event> events = modelEvent.getListBeforeLimit(MonthFragment.PERIOD);
				int subscription = activity.getAssociationViewModel().getSubscriberList().size();

				// si aucune association n'est suivie
				if (subscription == 0) {
					information_no_subscribe.setVisibility(View.VISIBLE);
					information_no_event.setVisibility(View.INVISIBLE);
					recyclerView.setVisibility(View.INVISIBLE);

					// action sur le bouton pour aller sur le fragment d'abonnement
					information_no_subscribe.findViewById(R.id.information_no_subsciption_button).setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							NavHostFragment navHostFragment = (NavHostFragment) activity.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
							Objects.requireNonNull(navHostFragment).getNavController().navigate(R.id.nav_subscriber_association);
						}
					});
				}

				// si aucun événement n'est prévus
				else if (events.size() == 0) {
					information_no_subscribe.setVisibility(View.INVISIBLE);
					information_no_event.setVisibility(View.VISIBLE);
					recyclerView.setVisibility(View.INVISIBLE);
				}

				// sinon
				else {
					information_no_subscribe.setVisibility(View.INVISIBLE);
					information_no_event.setVisibility(View.INVISIBLE);
					recyclerView.setVisibility(View.VISIBLE);
					recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
					recyclerView.setAdapter(new EventRecyclerViewAdapter(events, activity));
				}
			}
		});
		return layout;
	}
}
